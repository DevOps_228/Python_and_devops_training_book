"""
Напишите функцию Python, принимающую строку в качестве аргумента и выводящую в консоль информацию о её регистре
"""

import re

def check_regiser(aboba = input("Input string ")):
    lowers = len(re.findall(r'[A-Z]', aboba))
    uppers = len(re.findall(r'[a-z]', aboba))
    numbers = len(re.findall(r'[0-9]', aboba))
    symbols = len(re.findall(r'[@_!#$%^&*()<>?/\|}{~:]', aboba))
    spaces = len(re.findall(r'[ ]', aboba))

    print(f'''Эта строка содержит следюущее количество:
    Символов нижнего регистра: {lowers}
    Символов вернхнего регистра: {uppers}
    Цифр: {numbers}
    Специальных символов: {symbols}
    Пробелов: {spaces}''')

check_regiser()

# def check_regiser(aboba = input("Input string ")):
#     lower = 0
#     upper = 0
#     for i in list(aboba):
#         if i.islower() == True:
#             lower += 1
#         elif i.isupper():
#             upper += 1
#     print(f'Эта строка содержит {lower} нижнего регистра {upper} верхнего регистра')
# check_regiser()