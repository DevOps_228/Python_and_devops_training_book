"""
Создайте с помощью библиотеки click утилиту командной строки, принимающую в качестве аргумента
название и выводящую его в случае если оно не нечинается с символа `p`
"""
import click
import re

@click.command()
@click.option('--name', help='Name that could be return, if it not contain "p" symbol')
def hello(name):
    if re.match(r'^p', name):
        pass
    else:
        click.echo(name)

if __name__ == '__main__':
    hello()